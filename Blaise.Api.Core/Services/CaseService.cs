﻿using System.Collections.Generic;
using System.Linq;
using Blaise.Api.Contracts.Models.Case;
using Blaise.Api.Core.Extensions;
using Blaise.Api.Core.Interfaces.Services;
using Blaise.Nuget.Api.Contracts.Enums;
using Blaise.Nuget.Api.Contracts.Interfaces;

namespace Blaise.Api.Core.Services
{
    public class CaseService : ICaseService
    {
        private readonly IBlaiseCaseApi _blaiseCaseApi;

        public CaseService(IBlaiseCaseApi blaiseCaseApi)
        {
            _blaiseCaseApi = blaiseCaseApi;
        }

        public List<string> GetCaseIds(string serverParkName, string instrumentName)
        {
            var caseStatusList = _blaiseCaseApi.GetCaseStatusList(instrumentName, serverParkName);

            return caseStatusList.Select(caseStatus => caseStatus.PrimaryKey).ToList();
        }

        public List<CaseStatusDto> GetCaseStatusList(string serverParkName, string instrumentName)
        {
            var caseStatusList = _blaiseCaseApi.GetCaseStatusList(instrumentName, serverParkName);
            var caseStatusDtoList = new List<CaseStatusDto>();

            foreach (var caseStatus in caseStatusList)
            {
                caseStatusDtoList.Add(

                    new CaseStatusDto
                    {
                        PrimaryKey = caseStatus.PrimaryKey,
                        Outcome = caseStatus.Outcome
                    });

            }

            return caseStatusDtoList;
        }

        public string GetPostCode(string serverParkName, string instrumentName, string caseId)
        {
            var caseRecord = _blaiseCaseApi.GetCase(caseId, instrumentName, serverParkName);

            return _blaiseCaseApi.GetFieldValue(caseRecord, FieldNameType.PostCode).ValueAsText;
        }

        public CaseDto GetCase(string serverParkName, string instrumentName, string caseId)
        {
            serverParkName.ThrowExceptionIfNullOrEmpty("serverParkName");
            instrumentName.ThrowExceptionIfNullOrEmpty("instrumentName");
            caseId.ThrowExceptionIfNullOrEmpty("caseId");

            var caseRecord = _blaiseCaseApi.GetCase(caseId, instrumentName, serverParkName);

            return new CaseDto
            {
                CaseId = _blaiseCaseApi.GetPrimaryKeyValue(caseRecord),
                FieldData = _blaiseCaseApi.GetRecordDataFields(caseRecord)
            };
        }

        public void CreateCase(string serverParkName, string instrumentName, string caseId,
            Dictionary<string, string> fieldData)
        {
            serverParkName.ThrowExceptionIfNullOrEmpty("serverParkName");
            instrumentName.ThrowExceptionIfNullOrEmpty("instrumentName");
            caseId.ThrowExceptionIfNullOrEmpty("caseId");
            fieldData.ThrowExceptionIfNullOrEmpty("fieldData");

            _blaiseCaseApi.CreateCase(caseId, fieldData, instrumentName, serverParkName);
        }

        public void UpdateCase(string serverParkName, string instrumentName, string caseId, Dictionary<string, string> fieldData)
        {
            serverParkName.ThrowExceptionIfNullOrEmpty("serverParkName");
            instrumentName.ThrowExceptionIfNullOrEmpty("instrumentName");
            caseId.ThrowExceptionIfNullOrEmpty("caseId");
            fieldData.ThrowExceptionIfNullOrEmpty("fieldData");

            _blaiseCaseApi.UpdateCase(caseId, fieldData, instrumentName, serverParkName);
        }

        public void DeleteCase(string serverParkName, string instrumentName, string caseId)
        {
            serverParkName.ThrowExceptionIfNullOrEmpty("serverParkName");
            instrumentName.ThrowExceptionIfNullOrEmpty("instrumentName");
            caseId.ThrowExceptionIfNullOrEmpty("caseId");

            _blaiseCaseApi.RemoveCase(caseId, instrumentName, serverParkName);
        }
    }
}